﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReedeAugust
{
    class Program
    {
        static void Main(string[] args)
        {
            // kust ma peaks pihta hakkama?
            // 1. asi defineerin omale massiivi
            int[,] massiiv = new int[12, 8];

            // 2. asi hakkan seda täitma juhuslike arvudega
            // selleks on vaja seda va Randomit, kellelt neid võtta

            Random r = new Random();

            // 3. nüüd tuleks teha tsükkel, mis täidab
            // OI APPI - aga siin on kahemõõtmeline ???
            // äkki peaks tegema KAKS tsüklit, üks mis käib mööda
            // ridu ja teine mööda veerge ja need üksteise sisse panema?

            // mis tsükkel siia sobiks?
            // foreach vist ei sobi?
            // while vist ka ei sobi
            // proovime for?

            // aga siis ta äkki ütleb, et tehke nüüd 12x12
            for (int i = 0; i < massiiv.GetLength(0) ; i++)
            {
                // nii saame kätte read
                // nüüd oleks vaja igasse lahtrisse midagi kirjutada
                // teeme siis teise tsükli siia sisse
                // huvitav, mis see i siin vea annab?
                // ahjah - seal üleval on juba i võtame siis j
                for (int j = 0; j < massiiv.GetLength(1); j++)
                {
                    // nüüd on meil i-reanumber, j-veerunumber
                    massiiv[i, j] = r.Next() % 100 + 1;
                }
               
            }

            // vaatame, mis sai - kas on massiiv arve täis?
            // kuidas? 
            // no trükime välja

            // ei nii ei saa
            // tsükime tsükliga
            // foreach (int arv in massiiv) Console.Write($" {arv}");
            // nii ei saa ju midagi aru
            // trükiks ridade kaupa - ehh
            // tühik tuleks vahjele panna?
            // no kuidas teha ridade kaupa - proovime jälle for
            Console.WriteLine();
            // oo äge - ta teeb pool tööd minu eest ära
            // vaata: ma ütlen for ja kaks korda tab

            for (int i = 0; i < massiiv.GetLength(0); i++)
            {
                // proovime uuesti
                // äge ta sai aru, et igal pool j
                for (int j = 0; j < massiiv.GetLength(1); j++)
                {
                    Console.Write($"\t{massiiv[i,j]}");
                }
                Console.WriteLine();
            }
            // ägge - nii on loetav, võtame selle foreachi ära hoopis
            // mõned arvud pikemad, mõned lähemad - tab on parem kui tühik

            // 5. nüüd pean selle va kõige suurema leidma
            // kle prooviks - keskmine oli ja sum oli, äkki on maks ka?
            // krt! - ei ole
            // aga äkki Listil on?
            // aga äkki guuglil on

            /*
            var arr = new int[2, 2] {{1,2}, {3, 4}};
            int max = arr.Cast<int>().Max();    //or Min
            
             * 
             */ 
            // mida ta siin mõtleb?
            // aga proovime
            int max = massiiv.Cast<int>().Max();
            // ÄGGE! Leidsingi
            Console.WriteLine($"suurim arv on {max}"); // ehhe töötab

            // 6. ta tahtis teada, kus reas, kus veerus see on??
            // hmm...
            // kas ma JÄLLE pean selle tsükli tegema - ei viitsi

            for (int i = 0; i < massiiv.GetLength(0); i++)
            {
                for (int j = 0; j < massiiv.GetLength(1); j++)
                {
                    if (massiiv[i,j] == max)
                    {
                        Console.WriteLine($"see on {i+1}. reas ja {j+1}. veerus");
                    }
                }
            }

            // NB! Tehtud :) - mina sain esimesena valmis
        
}
}
}
